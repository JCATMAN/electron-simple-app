function initMap() {
    let options = {
        center: {lat:6.1721787, lng:-75.4228077},
        zoom: 13
    }

    const map = new google.maps.Map(document.getElementById('map'),options);

    const marker = new google.maps.Marker({
        position: {lat:6.1721787, lng:-76.4228077},
        map:map
    });
}